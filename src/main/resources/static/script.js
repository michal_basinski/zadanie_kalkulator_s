(function() {
  
  var app = angular.module("salaryComputer", []);
  
  var CalculatorController = function($scope, $http) {

    $scope.calculateSalaryByCountryCode = function() {
        var countryCode = $scope.countryCode;
        var dailyWage = $scope.dailyWage;

        var request = {
            countryId =
        }

        var serviceUrl = "/calculator/";

        $http.get(serviceUrl).then(onSalaryComputeComplete, onError);
        $http.post(serviceUrl)
    }

    var onSalaryComputeComplete = function(response) {
      $scope.salaryResponse = response.data;
      $scope.showResult = true;
    };

    var onError = function(reason) {
      $scope.error = "Couldn't fetch NBP currency data";
      $scope.showError = true;
    };


  };
  app.controller("CalculatorController", CalculatorController);
  
}());