package org.s.calculator.conversions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ConversionFacade {

    private RestTemplate restTemplate;

    private String serviceUrl;

    public ConversionFacade(RestTemplate restTemplate, String serviceUrl) {
        this.restTemplate = restTemplate;
        this.serviceUrl = serviceUrl;
    }

    public Double convertToAmountInPLN(Double amount, String currencyName) {
        double convertedAmount = amount;
        if (!"PLN".equals(currencyName)) {
            ResponseEntity<ConversionServiceResponse> response = restTemplate.getForEntity(serviceUrl + currencyName, ConversionServiceResponse.class);
            convertedAmount = amount / response.getBody().getRates().get(0).getMid();
        }
        return convertedAmount;
    }
}
