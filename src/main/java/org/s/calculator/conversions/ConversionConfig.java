package org.s.calculator.conversions;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
class ConversionConfig {

    @Value("${nbp.api.url}")
    String serviceUrl;

    @Bean
    ConversionFacade conversionFacade(RestTemplate restTemplate) {
        return new ConversionFacade(restTemplate, serviceUrl);
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
