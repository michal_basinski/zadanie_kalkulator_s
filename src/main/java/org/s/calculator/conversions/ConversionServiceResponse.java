package org.s.calculator.conversions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConversionServiceResponse {
    List<ConversionRate> rates;

    public List<ConversionRate> getRates() {
        return rates;
    }

    public void setRates(List<ConversionRate> rates) {
        this.rates = rates;
    }
}
