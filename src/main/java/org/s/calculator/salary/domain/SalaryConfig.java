package org.s.calculator.salary.domain;

import org.s.calculator.conversions.ConversionFacade;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class SalaryConfig {

    @Bean
    SalaryFacade salaryFacade(CountryRepository countryRepository, ConversionFacade conversionFacade) {
        return new SalaryFacade(countryRepository, conversionFacade);
    }

}
