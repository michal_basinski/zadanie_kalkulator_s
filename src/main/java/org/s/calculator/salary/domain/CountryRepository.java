package org.s.calculator.salary.domain;

import org.springframework.data.jpa.repository.JpaRepository;

interface CountryRepository extends JpaRepository<Country, Long> {
}
