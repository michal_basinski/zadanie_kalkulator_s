package org.s.calculator.salary.domain;

import org.s.calculator.salary.dto.CountryDto;
import org.s.calculator.salary.dto.CountryDtoBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "COUNTRIES")
class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Double taxRate;

    private Double fixedCost;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "COUNTRY_CURRENCY_ID", nullable = false)
    private Currency currency;

    Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    Double getTaxRate() {
        return taxRate;
    }

    void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    Double getFixedCost() {
        return fixedCost;
    }

    void setFixedCost(Double fixedCost) {
        this.fixedCost = fixedCost;
    }

    Currency getCurrency() {
        return currency;
    }

    void setCurrency(Currency currency) {
        this.currency = currency;
    }

    CountryDto toEntity() {
        CountryDtoBuilder builder = new CountryDtoBuilder();

        CountryDto countryDto = builder
                .withId(this.id)
                .withName(this.name)
                .withCurrencyName(this.currency.currencyName)
                .withFixedCosts(this.fixedCost)
                .withTax(this.taxRate).build();

        return countryDto;
    }

    Double getMonthlySalary(Double dailyRate, int workingDays) {
        return (dailyRate * workingDays) * (1 - taxRate) - fixedCost;
    }
}
