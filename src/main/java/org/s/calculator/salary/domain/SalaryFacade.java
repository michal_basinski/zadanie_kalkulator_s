package org.s.calculator.salary.domain;

import org.s.calculator.conversions.ConversionFacade;
import org.s.calculator.salary.dto.ComputeSalaryRequest;
import org.s.calculator.salary.dto.CountryDto;
import org.s.calculator.salary.dto.SalaryDto;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SalaryFacade {

    private CountryRepository countryRepository;
    private final ConversionFacade conversionFacade;

    public SalaryFacade(CountryRepository countryRepository, ConversionFacade conversionFacade) {
        this.countryRepository = countryRepository;
        this.conversionFacade = conversionFacade;
    }

    public List<CountryDto> getCountries() {
        List<Country> countries = countryRepository.findAll();
        List<CountryDto> countryDtos = countries.stream().map(x -> x.toEntity()).collect(Collectors.toList());
        return countryDtos;
    }

    public SalaryDto getMonthlySalary(ComputeSalaryRequest computeSalaryRequest) {
        Optional<Country> searchResult = countryRepository.findById(computeSalaryRequest.getCountryId());
        Country country = searchResult.get();

        SalaryDto salaryDto = new SalaryDto();
        salaryDto.setOriginalCurrency(country.getCurrency().getCurrencyName());

        Double originalSalary = country.getMonthlySalary(computeSalaryRequest.getDailyRate(), 22);
        Double salaryInPln = conversionFacade.convertToAmountInPLN(originalSalary, country.getCurrency().getCurrencyName());

        salaryDto.setMonthlySalary(salaryInPln);
        salaryDto.setMonthlySalaryOriginal(originalSalary);


        return salaryDto;
    }
}
