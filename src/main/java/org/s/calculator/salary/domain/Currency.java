package org.s.calculator.salary.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "CURRENCIES")
class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String currencyName;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "currency")
    List<Country> countries;

    Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    String getCurrencyName() {
        return currencyName;
    }

    void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }
}
