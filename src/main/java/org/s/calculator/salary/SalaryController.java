package org.s.calculator.salary;

import org.s.calculator.conversions.ConversionFacade;
import org.s.calculator.salary.domain.SalaryFacade;
import org.s.calculator.salary.dto.ComputeSalaryRequest;
import org.s.calculator.salary.dto.CountryDto;
import org.s.calculator.salary.dto.SalaryDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SalaryController {

    private SalaryFacade salaryFacade;

    public SalaryController(SalaryFacade salaryFacade) {
        this.salaryFacade = salaryFacade;
    }

    @PostMapping("/calculator")
    public ResponseEntity getMonthlySalary(@RequestBody ComputeSalaryRequest request) {
        SalaryDto monthlySalary = salaryFacade.getMonthlySalary(request);
        return new ResponseEntity(monthlySalary, HttpStatus.OK);
    }

    @GetMapping("/calculator/countries")
    public ResponseEntity getCountries() {
        List<CountryDto> countries = salaryFacade.getCountries();
        return new ResponseEntity(countries, HttpStatus.OK);
    }

}
