package org.s.calculator.salary.dto;

public class CountryDto {
    private Long id;
    private String name;
    private Double tax;
    private Double fixedCosts;
    private String currencyName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getFixedCosts() {
        return fixedCosts;
    }

    public void setFixedCosts(Double fixedCosts) {
        this.fixedCosts = fixedCosts;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }
}
