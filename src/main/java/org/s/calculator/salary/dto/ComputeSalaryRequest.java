package org.s.calculator.salary.dto;

public class ComputeSalaryRequest {
    private Double dailyRate;
    private Long countryId;

    public ComputeSalaryRequest(Double dailyRate, Long countryId) {
        this.dailyRate = dailyRate;
        this.countryId = countryId;
    }

    public Double getDailyRate() {
        return dailyRate;
    }

    public Long getCountryId() {
        return countryId;
    }
}
