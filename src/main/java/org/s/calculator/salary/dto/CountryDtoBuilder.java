package org.s.calculator.salary.dto;

public class CountryDtoBuilder {
    private CountryDto countryDto;

    public CountryDtoBuilder() {
        this.countryDto = new CountryDto();
    }

    public CountryDtoBuilder withId(Long id) {
        countryDto.setId(id);
        return this;
    }

    public CountryDtoBuilder withName(String name) {
        countryDto.setName(name);
        return this;
    }

    public CountryDtoBuilder withCurrencyName(String currencyName) {
        countryDto.setCurrencyName(currencyName);
        return this;
    }

    public CountryDtoBuilder withFixedCosts(Double fixedCosts) {
        countryDto.setFixedCosts(fixedCosts);
        return this;
    }

    public  CountryDtoBuilder withTax(Double tax) {
        countryDto.setTax(tax);
        return this;
    }

    public CountryDto build() {
        return countryDto;
    }
}
