package org.s.calculator.salary.dto;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SalaryDto {
    private Double monthlySalary;
    private String currency;
    private Double monthlySalaryOriginal;
    private String originalCurrency;

    public Double getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(Double monthlySalary) {
        this.monthlySalary = monthlySalary;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getMonthlySalaryOriginal() {
        return monthlySalaryOriginal;
    }

    public void setMonthlySalaryOriginal(Double monthlySalaryOriginal) {
        this.monthlySalaryOriginal = monthlySalaryOriginal;
    }

    public String getOriginalCurrency() {
        return originalCurrency;
    }

    public void setOriginalCurrency(String originalCurrency) {
        this.originalCurrency = originalCurrency;
    }
}
