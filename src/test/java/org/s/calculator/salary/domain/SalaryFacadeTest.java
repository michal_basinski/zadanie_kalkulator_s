package org.s.calculator.salary.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.s.calculator.conversions.ConversionFacade;
import org.s.calculator.salary.dto.ComputeSalaryRequest;
import org.s.calculator.salary.dto.CountryDto;
import org.s.calculator.salary.dto.SalaryDto;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SalaryFacadeTest {

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private ConversionFacade conversionFacade;

    private SalaryFacade salaryFacade;

    @Before
    public void setUp() {
        salaryFacade = new SalaryFacade(countryRepository, conversionFacade);
        SalaryTestUtils salaryTestUtils = new SalaryTestUtils();
        when(countryRepository.findAll()).thenReturn(Arrays.asList(salaryTestUtils.getPoland(), salaryTestUtils.getGermany()));
        when(countryRepository.findById(1l)).thenReturn(java.util.Optional.ofNullable(salaryTestUtils.getPoland()));
        when(conversionFacade.convertToAmountInPLN(anyDouble(), anyString())).then(returnsFirstArg());
    }

    @Test
    public void shouldReturnPolandAndGermanyDtos() {
        //when
        List<CountryDto> countries = salaryFacade.getCountries();

        //then
        assertTrue(countries.size() == 2);
    }

    @Test
    public void shouldReturn4146MonthlySalaryInPoland() {
        //given
        final Double expectedMonthlySalary = 4146d;
        final Double dailyRate = 300d;
        final long polandId = 1;
        final ComputeSalaryRequest computeSalaryRequest = new ComputeSalaryRequest(dailyRate, polandId);

        //when
        SalaryDto monthlySalary = salaryFacade.getMonthlySalary(computeSalaryRequest);

        //then
        assertEquals(expectedMonthlySalary, monthlySalary.getMonthlySalary());
    }
}