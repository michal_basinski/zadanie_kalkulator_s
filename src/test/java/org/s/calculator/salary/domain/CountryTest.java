package org.s.calculator.salary.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CountryTest {
    @Test
    public void shouldReturn4164MonthlySalaryIfWorkingFor22DaysInPoland() {
        //given
        final Double expectedMonthlySalary = 4146d;
        final Double dailyRate = 300d;
        final int workingDays = 22;
        final Country poland = new Country();
        poland.setFixedCost(1200d);
        poland.setTaxRate(0.19);

        //when
        Double monthlySalary = poland.getMonthlySalary(dailyRate, workingDays);

        //then
        assertEquals(expectedMonthlySalary, monthlySalary);
    }

}