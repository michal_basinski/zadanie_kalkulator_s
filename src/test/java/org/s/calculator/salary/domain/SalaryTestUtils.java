package org.s.calculator.salary.domain;

import org.s.calculator.conversions.ConversionRate;
import org.s.calculator.conversions.ConversionServiceResponse;

import java.util.Arrays;

class SalaryTestUtils {
    Country getGermany() {
        Currency eur = new Currency();
        eur.setId(2l);
        eur.setCurrencyName("EUR");

        Country germany = new Country();
        germany.setId(2l);
        germany.setName("DE");
        germany.setTaxRate(0.20d);
        germany.setFixedCost(800d);
        germany.setCurrency(eur);
        return germany;
    }

    Country getPoland() {
        Currency pln = new Currency();
        pln.setId(1l);
        pln.setCurrencyName("PLN");

        Country poland = new Country();
        poland.setId(1l);
        poland.setName("PL");
        poland.setFixedCost(1200d);
        poland.setTaxRate(0.19d);
        poland.setCurrency(pln);
        return poland;
    }
//
//    ConversionServiceResponse getConversionServiceResponse() {
//        ConversionServiceResponse response = new ConversionServiceResponse();
//        ConversionRate conversionRate = new ConversionRate();
//        conversionRate.setMid();
//        response.setConversionRates(Arrays.asList());
//
//        return response;
//    }
}
